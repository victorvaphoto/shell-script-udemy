#!/bin/bash

echo hello

WORD='script'

#use double quote to allow variable expansion 
echo "hello you $WORD"

echo "hello this works tooo ${WORD}"

ENDING='ed'

echo "this is ${WORD} ${ENDING}"
