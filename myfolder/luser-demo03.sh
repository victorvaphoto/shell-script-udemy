#!/bin/bash

#display the UID and user name of the user exeucting this script
echo "your UID is ${UID}"

UID_TO_TEST_FOR='1000'

if [[ "${UID}" -ne "${UID_TO_TEST_FOR}" ]]
then
    echo "your UID does not match ${UID_TO_TEST_FOR}"
    exit 1
fi


USER_NAME=$(id -un)

# holds exist status of last command
if [[ "${?}" -ne 0 ]]
then 
    echo 'id command did not run'
    exit 1
fi
   

echo "your user name is ${USER_NAME}"

USER_NAME_TO_TEST_FOR='vagrant'

if [[ "${USER_NAME}" = "${USER_NAME_TO_TEST_FOR}" ]]
then
    echo "your user name matches ${USER_NAME_TO_TEST_FOR}"
else
    echo "your user name does not match ${USER_NAME_TO_TEST_FOR}"    
fi 

exit 0
